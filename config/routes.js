/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {
  /***************************************************************************
   *                                                                          *
   * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
   * etc. depending on your default view engine) your home page.              *
   *                                                                          *
   * (Alternatively, remove this and add an `index.html` file in your         *
   * `assets` directory)                                                      *
   *                                                                          *
   ***************************************************************************/


    /*coordenadas parroquias  */
  "get /api/coordenadasparroq/find/:parroquia_id":
    "CoordenadasParroqController.find",
  "get /api/coordenadasparroq/find1/:parroquia_id":
    "CoordenadasParroqController.find1",
  "get /api/coordenadasparroq/find2/:parroquia_id":
    "CoordenadasParroqController.find2",

  /*delitos api*/
  "get /api/delitos": "DelitosController.get",

  /*Estados api*/
  "get /api/estados": "EstadosController.get",

  /*delitosdetalles api*/
  "get /api/delitosdetalles": "DelitosDetallesController.get",
  "post /api/delitosdetalles/create": "DelitosDetallesController.create",
  "put /api/delitosdetalles/update": "DelitosDetallesController.update",
  "get /api/delitosdetalles/find/:delito_id": "DelitosDetallesController.find",

  /*Municipios api*/
  "get /api/municipios": "MunicipiosController.get",

  /*Parroquias api*/
  "get /api/parroquias": "ParroquiasController.get",

  /*Profesiones api*/
  "get /api/profesiones": "ProfesionesController.get",

  /*Sucesos api*/
  "get /api/sucesos": "SucesosController.get",
  "get sucesos/suceso?": "SucesosController.get1",
  "get /api/sucesos/list": "SucesosController.list",
  "get /api/sucesos/list1?":
    "SucesosController.list1" /* api/sucesos/list1?skip=0&top=4&sexo=m&tipo_arma=1&profesion=guardia%20nacional&titulo=GNB  */,
  "get /api/sucesos/list2?":
    "SucesosController.list2" /* api/sucesos/list2?skip=0&top=4&sexo=m&tipo_arma=1&titulo=GNB  */,
  "get /api/sucesos/list22?":
    "SucesosController.list22" /* api/sucesos/list2?skip=0&top=4&municipioid=4  */,
  "get /api/sucesos/list3?":
    "SucesosController.list3" /* http://192.168.0.106:1337/api/sucesos/list3?page=1&per_page=3*/,
  "get /api/sucesos/list33?":
    "SucesosController.list33" /* http://192.168.0.106:1337/api/sucesos/list3?page=1&per_page=3*/,
  "get /api/sucesos/list4?":
    "SucesosController.list4" /* http://192.168.0.106:1337/api/sucesos/list3?page=1&per_page=3*/,
  "get /api/sucesos/list44?":
    "SucesosController.list44" /* http://34.229.201.249:1337/api/sucesos/list44?page=0&results=2*/,
  "get /api/sucesos/paginas?":
    "SucesosController.paginas" /* http://192.168.0.106:1337/api/sucesos/list3?page=1&per_page=3*/,
  "get /api/sucesos/sql": "SucesosController.sql",
  "get /api/sucesos/sql_?": "SucesosController.sql_",
  "put /api/sucesos/update": "SucesosController.update",
  "get /api/sucesos/find/:suceso_id": "SucesosController.find",
  "get /api/sucesos/find1/:suceso_id": "SucesosController.find1",
  "get /api/sucesos/findsql/:suceso_id": "SucesosController.findsql", //http://34.229.201.249:1337/api/sucesos/findsql/2236
  "get /api/sucesos/findall/:search": "SucesosController.findAll", //http://34.229.201.249:1337/api/sucesos/findall/varios values diferentes
  "get /api/sucesos/findall?": "SucesosController.findAll", ////http://34.229.201.249:1337/api/sucesos/findall8/?search=  Solo 8 registros
  "get /api/sucesos/findHomiMunicipios/":
    "SucesosController.findHomiMunicipios", //http://34.229.201.249:1337/api/sucesos/findsql/
  "get /api/sucesos/findall8/:search": "SucesosController.findAll8", //Solo 8 registros
  "get /api/sucesos/findall8?": "SucesosController.findAll8", // //http://34.229.201.249:1337/api/sucesos/findall8/?search=  Solo 8 registros
  "GET /api/sucesos/search?": "SucesosController.findAll8_", //le agregue hits como nombre de datos,
  "get /api/sucesos/findHomiMunicipioId/:municipio_id":
    "SucesosController.findHomiMunicipioId", //http://34.229.201.249:1337/api/sucesos/findHomiMunicipioId/3
  "get /api/sucesos/cuantos_dame/:suceso_id": "SucesosController.cuantos_dame",
  "get /api/sucesos/homianoparrocar": "SucesosController.homianoparrocar",
  "post /api/sucesos/create": "SucesosController.create", //11-12-19 Crear un suceso
  "delete /api/sucesos/deletesuceso/:suceso_id":
    "SucesosController.deletesuceso",
  "put /api/sucesos/updatesuceso": "SucesosController.updatesuceso",

  "GET /api/sucesos/sucesosall": "SucesosController.getall",
  "GET /api/sucesos/AcuAnoActual2": "SucesosController.AcuAnoActual2", //un solo objeto

  //To react native
  "get /api/sucesos/paginado": "SucesosController.paginado1", //para news
  "get /api/sucesos/acuanos": "SucesosController.acuAnos", //para news
  //Estadisticas
  "get /api/sucesos/AcuAnoActual": "SucesosController.AcuAnoActual", //varios objetos, arrays
  "get /api/sucesos/AcuAnoActual1": "SucesosController.AcuAnoActual1", //un solo objeto, array
  "get /api/sucesos/AcuAnoActualMesantes":
    "SucesosController.AcuAnoActualMesantes", //
  "get /api/sucesos/AcuAnoActualParro": "SucesosController.AcuAnoActualParro", //por parroquias año actu1al varios objetos
  "get /api/sucesos/AcuAnoActualParro1": "SucesosController.AcuAnoActualParro1", //por parroquias año actual  un solo objeto,  directo

  /*        SucesosController    index           ¨paginas web en servidor*/

  "/": { controller: "HomeController", action: "index" },

  "/about": {
    controller: "AboutController",
    action: "index",
  },

  "/sucesos": { controller: "SucesosController", action: "index" },
  "/paginados": { controller: "SucesosController", action: "paginado1" },
  "/suceso?": { controller: "SucesosController", action: "get1" },

  /*Users api*/
  "get /api/users": "UsersController.get",
  "get /api/users1": "UsersController.get1",
  "post /api/users/email": "UsersController.findemail",

  "get /api/usersm": "UsersMController.get",
  "post /api/users/login": "UsersController.login",
  "post /api/users/create": "UsersController.create", // countryId







};
