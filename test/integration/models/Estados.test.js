var util = require('util');

describe('Estados (model)', function () {

  describe('#findBestEstados()', function () {
    it('should return 5 Estados', function (done) {
      Estados.find()
        .then(function (BestEstados) {

          if (BestEstados.length !== 5) {
            console.log(BestEstados.length);
            return done(new Error(
              'Should return exactly 5 estados -- the estados ' +
              'from our test fixtures who are considered the "best".  ' +
              'But instead, got: ' + util.inspect(BestEstados, { depth: null }) + ''
            ));
          }//-•

          return done();

        })
        .catch(done);
    });
  });

});
