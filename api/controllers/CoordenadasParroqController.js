/**
 * CoordenadasParroqController
 *
 * @description :: Server-side logic for managing CoordenadasParroqController
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var validate = require("validate.js");
var moment = require('moment-timezone');
var _ = require('lodash');
moment.locale('es');
moment.tz.setDefault('America/Venezuela');



module.exports = {
	
  	find: function (req, res) {

  		var values = req.allParams();
  		//console.log('suceso '+values.suceso_id);

		CoordenadasParroquias.find({parroquia_id: values.parroquia_id}).populate('parroquia_id').exec(function (err, response){
			if (err) return res.serverError(err);
			return res.json(200,response);
		});

	},

    find1: function (req, res) {
        var values = req.allParams();
        async.parallel([
            function(callback) {
                Parroquias.find({parroquia_id: values.parroquia_id}).exec(function (err, parroquia){
                    if(err) console.log(err);
                    callback(null, parroquia);
                });
            },
            function(callback) {
                CoordenadasParroquias.find({parroquia_id: values.parroquia_id}).populate('parroquia_id').exec(function (err, response){
                    if(err) console.log(err);
                    callback(null, response);
                });
            }
        ],
        function(err, results) {
            return res.json(200,{
                parroquia: results[0],
                latitud: results[1]
            });
        });
    },

    find2: function (req, res) {
        var values = req.allParams();
        var arra = [];
        var datas = [];
        async.parallel([
            function(callback) {
                Parroquias.find({parroquia_id: values.parroquia_id}).exec(function (err, tot_registros){
                    if(err) console.log('error1'+err);
                    _.forEach(tot_registros, function(s) {
                        parroquia_id = s.parroquia_id;
                        parroquia = s.descripcion;
                    });
                    callback();
                });
                
            },
            function(callback) {
                CoordenadasParroquias.find({parroquia_id: values.parroquia_id}).exec(function (err, coordenadas){
                    if(err) console.log(err);
                    _.forEach(coordenadas, function(s) {
                        datas.push({'lat' : s.lat, 'lng' : s.lng});
                    });
                    callback(); 
                });
            }
        ],
        function(err) {
            return res.json(200,{
                "parroquia_id":parroquia_id,
                "parroquia":parroquia,
                coordenadas: datas
            });

        });

        

    },


};

