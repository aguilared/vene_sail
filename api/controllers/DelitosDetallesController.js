/**
 * DelitosDetallesController
 *
 * @description :: Server-side logic for managing studentlevel
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	get: function (req, res) {

		var query = {};		

		DelitosDetalles.find().sort('delito_detalle_id desc').exec(function (err, results){
			if (err) return res.serverError(err);
			return res.json(200,results);
		});

	},

  	
	
};

