/**
 * sucesosController
 *
 * @description :: Server-side logic for managing studentlevel
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var validate = require("validate.js");
var moment = require('moment-timezone');
var _ = require('lodash');
moment.locale('es');
moment.tz.setDefault('America/Venezuela');

var municipio_id = 3;
var delito_deta = 7;

module.exports = {

	index: function(req, res){
            var arra = [];
            var arra1 = [];
            var arra2 = [];
            var mes = moment().format('MM');

            async.parallel([
                function(callback) {
                    var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant, YEAR(fecha_suceso) AS ano, MONTH(fecha_suceso) AS mes '
                        + 'FROM sucesos AS s '
                        + 'WHERE YEAR(fecha_suceso) = YEAR(now()) AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
                        +  'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
                    //Usado solo cuando es mes de Enero
                    if (mes=='01') {
                    	var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant, YEAR(fecha_suceso) AS ano, MONTH(fecha_suceso) AS mes '
                        + 'FROM sucesos AS s '
                        + 'WHERE YEAR(fecha_suceso) = YEAR(now())-1 AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
                        +  'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
                    };
                    //console.log(query);
                    Sucesos.query(query, function(err, tot_homi_mes) {
                        if(err) console.log('error1'+err);
                        _.forEach(tot_homi_mes, function(s) {
                            arra.push({'homi_mes_ant' : s.homi_mes_ant,'ano' : s.ano,'mes' : s.mes});
                        });
                        callback();
                    });

                },
                function(callback) {
                    var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
                        + 'dd.descripcion as detalle_delito, titulo, fuente, '
                        + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
                        + 'p.descripcion AS profesion, tipo_arma,'
                        + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
                        + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena, created_at, updated_at '
                        + 'FROM sucesos AS s '
                        + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
                        + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
                        + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
                        + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
                        + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
                        + 'ORDER BY fecha_suceso DESC ';
                    //console.log(query);
                    Sucesos.query(query, function(err, sucesos) {
                        if(err) console.log('error2'+err);
                        _.forEach(sucesos, function(s) {
                            var day = moment(s.fecha_suceso,'YYYY-MM-DD');
                            arra1.push({'suceso_id' : s.suceso_id, 'fecha_suceso' : day.format("YYYY-MM-DD"), 'titulo' : s.titulo, 'nombre_victima' : s.nombre_victima});
                        });
                        callback();
                    });

                },
                function(callback) {
                    var query = 'SELECT YEAR(fecha_suceso) AS ano, MONTH(fecha_suceso) AS mes, '
                        + 'SUM(CASE WHEN (YEAR(fecha_suceso)= YEAR(now()) ) THEN 1 ELSE 0 END) AS total, '
                        + 'SUM(CASE WHEN (MONTH(fecha_suceso) = MONTH(now()) ) THEN 1 ELSE 0 END) AS tot_mes, '
                        + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 1) THEN 1 ELSE 0 END) AS ene, '
						            + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 2) THEN 1 ELSE 0 END) AS feb, '
                        + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 3) THEN 1 ELSE 0 END) AS mar, '
                        + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 4) THEN 1 ELSE 0 END) AS abr, '
                        + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 5) THEN 1 ELSE 0 END) AS may, '
                        + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 6) THEN 1 ELSE 0 END) AS jun, '
                        + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 7) THEN 1 ELSE 0 END) AS jul, '
                        + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 8) THEN 1 ELSE 0 END) AS ago, '
                        + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 9) THEN 1 ELSE 0 END) AS sep, '
                        + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 10) THEN 1 ELSE 0 END) AS oct, '
                        + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 11) THEN 1 ELSE 0 END) AS nov, '
                        + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 12) THEN 1 ELSE 0 END) AS dic '
                        + 'FROM sucesos AS s '
                        + 'WHERE YEAR(fecha_suceso) = YEAR(now()) '
                        +  'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
                    //console.log(query);
                    Sucesos.query(query, function(err, tot_homi_mes) {
                        if(err) console.log('error1'+err);
                        var fecha = moment().format('DD-MM-YYYY');
                        _.forEach(tot_homi_mes, function(s) {
                            arra2.push({'fecha' : fecha,'ano' : s.ano,'mes' : s.mes,'total' : s.total,'tot_mes' : s.tot_mes,'ene' : s.ene,'feb' : s.feb,'mar' : s.mar,'abr' : s.abr,'may' : s.may,'jun' : s.jun,'jul' : s.jul,'ago' : s.ago,'sep' : s.sep,'oct' : s.oct,'nov' : s.nov,'dic' : s.dic});
                        });
                        callback();
                    });

                },
            ],
                function(err, results) {
                    //console.log(arra[0]);
                    res.view({
                    title: 'Venezuela Segura',
                    title1: 'Sucesos',
                    municipio: 'Caroni',
                    estado: 'Bolivar',
                    homi_mes_antes: arra,
                    acumu_meses: arra2,
                    sucesos: arra1

                });
            });

    },




	get: function (req, res) {

		Sucesos.find().populate('delito_id').populate('delito_detalle_id').exec(function (err, response){
			if (err) return res.serverError(err);

			return res.json(200,response);
		});

	},



	get1: function (req, res) {
		var values = req.allParams();
		var suceso = values.suceso;
		//console.log('llego'+values);
		//if (req.isAjax) var suceso=455;

		var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
		+ 'dd.descripcion as detalle_delito, titulo, fuente, '
		+ 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
		+ 'p.descripcion AS profesion, tipo_arma,'
		+ 'm.descripcion as municipio, pa.descripcion AS parroquia, '
		+ 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena, created_at, updated_at '
		+ 'FROM sucesos AS s '
		+ 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
		+ 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
		+ 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
		+ 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
		+ 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
		+ 'WHERE suceso_id =' + suceso + '';

		Sucesos.query(query, function(err, suceso) {

			if(err) return res.redirect('/');
			var array = [];

	        _.forEach(suceso, function(s) {

	        	var day = moment(s.fecha_suceso,'YYYY-MM-DD');
	        	array.push({'suceso_id' : s.suceso_id, 'titulo' : s.titulo,
	        		'dia' : day.format("DD-MM-YYYY"), 'hora' : s.hora, 'fuente' : s.fuente,
	        		'nombre_victima' : s.nombre_victima, 'edad' : s.edad, 'sexo' : s.sexo, 'detalle_delito' : s.detalle_delito, 'municipio' : s.municipio,
	        		'parroquia' : s.parroquia, 'mi_resena' : s.mi_resena});

	        });

			//console.log(array[0]);
			res.view('sucesos/suceso', {
		        title: 'Suceso',
		        layout: null,
		        suceso: array
		    });


		});

	},


	list: function (req, res) {

		Sucesos.find().exec(function (err, response){
			if (err) return res.serverError(err);

			return res.json(200,response);
		});

	},


	list1: function (req, res) {

		var values = req.allParams()

        //console.log('Skip '+values.skip+ ' Top '+values.top);
        var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
		+ 'dd.descripcion as detalle_delito, titulo, fuente, '
		+ 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
		+ 'p.descripcion AS profesion, tipo_arma,'
		+ 'm.descripcion as municipio, pa.descripcion AS parroquia, '
		+ 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena, created_at, updated_at '
		+ 'FROM sucesos AS s '
		+ 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
		+ 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
		+ 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
		+ 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
		+ 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
		+ 'WHERE sexo ="' + values.sexo + '" AND tipo_arma ="' + values.tipo_arma + '" '
		+ 'AND p.descripcion ="' + values.profesion + '" '
		+ 'ORDER BY fecha_suceso DESC '
		+ 'LIMIT ' + values.skip + ',' + values.top;

		//console.log(query);
		Sucesos.query(query, function(err, sucesos) {
	    			if(err) console.log(err);
	    			return res.json(200,sucesos);
				});

	},

 	//por titulo2
	list2: function (req, res) {

		var values = req.allParams()

        //console.log('Skip '+values.skip+ ' Top '+values.top);
        var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
		+ 'dd.descripcion as detalle_delito, titulo, fuente, '
		+ 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
		+ 'p.descripcion AS profesion, tipo_arma,'
		+ 'm.descripcion as municipio, pa.descripcion AS parroquia, '
		+ 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena, created_at, updated_at '
		+ 'FROM sucesos AS s '
		+ 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
		+ 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
		+ 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
		+ 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
		+ 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
		+ 'WHERE titulo LIKE "%' + values.titulo + '%" '
		+ 'ORDER BY fecha_suceso DESC '
		+ 'LIMIT ' + values.skip + ',' + values.top;

		console.log(query);
		Sucesos.query(query, function(err, sucesos) {
	    			if(err) console.log(err);
	    			return res.json(200,sucesos);
				});

	},




	sql: function (req, res) {

		var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
		+ 'dd.descripcion as detalle_delito, titulo, fuente, '
		+ 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
		+ 'p.descripcion AS profesion, tipo_arma,'
		+ 'm.descripcion as municipio, pa.descripcion AS parroquia, '
		+ 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena, created_at, updated_at '
		+ 'FROM sucesos AS s '
		+ 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
		+ 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
		+ 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
		+ 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
		+ 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
		+ 'ORDER BY fecha_suceso DESC';
		//console.log(query);
		Sucesos.query(query, function(err, sucesos) {
	    			if(err) console.log(err);
	    			return res.json(200,sucesos);
				});

	},


  	create: function (req, res) {

  		var values = req.allParams();

  		if(!values.titulo){
  			return res.json(400, {
	      		error: 'data_missing',
				error_description: 'The titulo is required',
	      	});
  		}
  		Sucesos.create({titulo : values.titulo}).exec(function (err, titulo){
	  		if (err) return res.serverError(err);
	  		return res.json(200,titulo);
		});

  	},

  	update: function (req, res) {

  		var values = req.allParams();
  		console.log('suceso '+values.suceso_id);
  		if(!values.suceso_id || !values.titulo){
  			return res.json(400, {
	      		error: 'data_missing',
				error_description: 'The suceso_id and titulo is required',
	      	});
  		}
  		Sucesos.update({suceso_id : values.suceso_id},{titulo : values.titulo}).exec(function (err, response){
	  		if (err) return res.serverError(err);
	  		return res.json(200,response);
		});

  	},

  	find: function (req, res) {

  		var values = req.allParams();
  		//console.log('suceso '+values.suceso_id);

		Sucesos.findOne({suceso_id: values.suceso_id}).exec(function (err, response){
			if (err) return res.serverError(err);

			return res.json(200,response);
		});

	},

	find1: function (req, res) {

  		var values = req.allParams();
  		//console.log('suceso '+values.suceso_id);

		Sucesos.findOne({suceso_id: values.suceso_id}).populate('delito_id').populate('delito_detalle_id').exec(function (err, response){
			if (err) return res.serverError(err);

			return res.json(200,response);
		});

	},

	findsql: function (req, res) {

  		var values = req.allParams();
  		var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
		+ 'dd.descripcion as detalle_delito, titulo, fuente, '
		+ 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
		+ 'p.descripcion AS profesion, tipo_arma,'
		+ 'm.descripcion as municipio, pa.descripcion AS parroquia, '
		+ 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena, created_at, updated_at '
		+ 'FROM sucesos AS s '
		+ 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
		+ 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
		+ 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
		+ 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
		+ 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
		+ 'WHERE suceso_id ="' + values.suceso_id + '"';
  		//console.log('suceso '+values.suceso_id);

		Sucesos.query(query, function(err, suceso) {

			if (err) return res.serverError(err);

			return res.json(200,suceso);
		});

	},


};



