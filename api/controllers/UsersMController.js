/**
 * sucesosController
 *
 * @description :: Server-side logic for managing studentlevel
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var moment = require('moment-timezone');
var _ = require('lodash');

moment.locale('es');
moment.tz.setDefault('America/Venezuela');

module.exports = {
	get: function (req, res) {
		UsersM.find().exec(function (err, response){
			if (err) return res.serverError(err);
      console.log(response);
			return res.json(200,response);
		});

  },
};
