/**
 * sucesosController
 *
 * @description :: Server-side logic for managing studentlevel
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var validate = require("validate.js");
var moment = require('moment-timezone');
var _ = require('lodash');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
//const Users = require("../models/Users");
var bcrypt = require('bcryptjs');

moment.locale('es');
moment.tz.setDefault('America/Venezuela');

module.exports = {

	get: function (req, res) {

		Users.find().exec(function (err, response){
			if (err) return res.serverError(err);
      console.log(response);
			return res.json(200,response);
		});

  },
	get1: function (req, res) {

		Users.find().exec(function (err, response){
			if (err) return res.serverError(err);
      console.log(response);
      return res.json(200, {
        username: response.name,
        email: response.email,
        password: response.password,
        remenber_token: '123456'
      });
		});

  },
  findemail: function (req, res) {
    var values = req.allParams();
    Users.findOne({ email: values.email }).exec(function (err, user) {
      if (!user) {
        return res.json(400, {
          error: 'user_do_not_exist',
          error_description: 'The User do not exist'
        });
      }
      var token = jwt.sign({ UserID: user.password }, 'secret', { expiresIn: '1h' });
      var decoded = jwt.decode(token, { complete: true });
      return res.json(200, {
        username: user.name,
        email: user.email,
        password: user.password,
        remenber_token: token,
        expires_in: decoded.payload.exp,
      });
    });
  },
  login: function (req, res) {
    var values = req.allParams();
    console.log("Valuess", values);
    if (!values.email || !values.password)
      return res.badRequest("Email and password required");


    Users.findOne({ email: values.email })
      .then((user) => {
        if (!user) return res.notFound();
        //directo sin ir al modelo
        bcrypt.compare(values.password, user.password, (err, res) => {
          console.log("entro", res, err);
          if (err) return err;
          console.log("Resp?", res);
          if (!res) console.log("no match", res);
        });

        Users.comparePassword(values.password, user.password)
          .then(() => {
            //return res.send({ token: jwToken.issue({ id: user.id }) });
            return res.send({ jwt: jwToken.issue({ id: user.id }) });
          })
          .catch((err) => {
            return res.forbidden();
          });
      })
      .catch((err) => {
        sails.log.error(err);
        return res.serverError();
      });


  },
  create(req, res) {
    var values = req.allParams();
    console.log("Values", values);
    if (values.password !== values.confirmPassword) return res.badRequest("Password not the same");

    Users.create({
      email: values.email,
      password: values.password,
      name: values.name
      //etc...
    })
      .then((user) => {
        res.send({ token: jwToken.issue({ id: user.id }) }); // payload is { id: user.id}
      })
      .catch((err) => {
        sails.log.error(err);
        return res.serverError("Something went wrong");
      });
  },

};
