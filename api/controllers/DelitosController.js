/**
 * delitosController
 *
 * @description :: Server-side logic for managing studentlevel
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	get: function (req, res) {

		var query = {};		

		Delitos.find().sort('delito_id desc').exec(function (err, results){
			if (err) return res.serverError(err);
			return res.json(200,results);
		});

	},

  	create: function (req, res) {

  		var values = req.allParams();

  		if(!values.descripcion){
  			return res.json(400, {
	      		error: 'data_missing',
				error_description: 'The descripcion is required',
	      	});
  		}
  		delitos.create({descripcion : values.descripcion}).exec(function (err, descripcion){
	  		if (err) return res.serverError(err);
	  		return res.json(200,descripcion);
		});

  	},

  	update: function (req, res) {

  		var values = req.allParams();

  		if(!values.delito_id || !values.descripcion){
  			return res.json(400, {
	      		error: 'data_missing',
				error_description: 'The delito_id and descripcion is required',
	      	});
  		}
  		delitos.update({delito_id : values.delito_id},{descripcion : values.descripcion}).exec(function (err, descripcion){
	  		if (err) return res.serverError(err);
	  		return res.json(200,descripcion);
		});

  	},
	
};

