/**
 * HomeController
 *
 * @description :: Server-side logic for managing homes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var validate = require("validate.js");
var moment = require('moment-timezone');
var _ = require('lodash');
moment.locale('es');
moment.tz.setDefault('America/Venezuela');
var municipio_id = 3;
var delito_deta = 7;

module.exports = {
    
    index: function(req, res){
            var arra = [];
            var arra1 = [];
            async.parallel([
                function(callback) {
                    var query = 'SELECT COUNT(fecha_suceso) AS homi_mes, ' 
                        + '(SELECT COUNT(fecha_suceso) '    
                        + 'FROM sucesos AS s '
                        + 'WHERE YEAR(fecha_suceso) = YEAR(now())-1 AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
                        +  'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + ') AS homi_mes_ant '
                        + 'FROM sucesos AS s '
                        + 'WHERE YEAR(fecha_suceso) = YEAR(now()) AND MONTH(fecha_suceso) = MONTH(now()) '
                        +  'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
                    //echo 
                    Sucesos.query(query, function(err, tot_homi_mes) {
                        if(err) console.log('error1'+err);
                        _.forEach(tot_homi_mes, function(s) {
                            arra.push({'homi_mes' : s.homi_mes,'homi_mes_ant' : s.homi_mes_ant}); 
                        });
                        callback();
                    });

                },
                function(callback) {
                    var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
                        + 'dd.descripcion as detalle_delito, titulo, fuente, '
                        + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
                        + 'p.descripcion AS profesion, tipo_arma,'
                        + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
                        + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
                        + 'FROM sucesos AS s '
                        + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
                        + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
                        + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
                        + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
                        + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
                        + 'ORDER BY fecha_suceso DESC ';
                    //console.log(query);
                    Sucesos.query(query, function(err, sucesos) {
                        if(err) console.log('error2'+err);
                        _.forEach(sucesos, function(s) {
                            var day = moment(s.fecha_suceso,'YYYY-MM-DD');   
                            arra1.push({'suceso_id' : s.suceso_id, 'fecha_suceso' : day.format("YYYY-MM-DD"), 'titulo' : s.titulo, 'nombre_victima' : s.nombre_victima});
                        });
                        callback();
                    });

                },
            ],
                function(err, results) {
                    //var homi_mes_ant = arra1[0]:
                    console.log(arra1[0]);
                    //console.log(homi_mes_ant);
                    res.view({
                    title: 'Venezuela Segura',
                    title1: 'Sucesos',
                    municipio: 'Caroni, Bitacora de Sucesos ',
                    estado: 'Bolivar',
                    homicidios: arra,
                    sucesos: arra1
                    //homicidiosMesAnt: arra[0].1

                    //results[0].tot_homi_mes
                    //homicidiosMesAnt: results[1].tot_homi_mes_ant,
                });
            });
            

    },
    
	
	


};

