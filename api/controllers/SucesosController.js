/**
 * sucesosController
 *
 * @description :: Server-side logic for managing sucesos
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var validate = require("validate.js");
var moment = require('moment-timezone');
var _ = require('lodash');
moment.locale('es');
moment.tz.setDefault('America/Venezuela');
const ano = moment().format("YYYY");
const ano_1 = moment()
  .subtract(1, "years")
  .calendar();


var municipio_id = 3;
var delito_deta = 7;

var parro_cacha = 731;
var parro_chi = 732;
var parro_dalla = 733;
var parro_once = 734;
var parro_pozo = 735;
var parro_simon = 736;
var parro_unare = 737;
var parro_uni = 738;
var parro_vista = 739;
var parro_yoco = 7310;

module.exports = {

	index: function(req, res){
    var arra = [];
    var arra1 = [];
    var arra2 = [];
    var mes = moment().format('MM');

    async.parallel([
        function(callback) {
            var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant '
                + 'FROM sucesos AS s '
                + 'WHERE YEAR(fecha_suceso) = YEAR(now()) AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
                +  'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
            //Usado solo cuando es mes de Enero
            if (mes=='01') {
              var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant '
                + 'FROM sucesos AS s '
                + 'WHERE YEAR(fecha_suceso) = YEAR(now())-1 AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
                +  'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
            };
            //console.log(query);
            Sucesos.query(query, function(err, tot_homi_mes) {
                if(err) console.log('error1'+err);
                _.forEach(tot_homi_mes, function(s) {
                    arra.push({'homi_mes_ant' : s.homi_mes_ant,'ano' : s.ano,'mes' : s.mes});
                });
                callback();
            });

        },
        function(callback) {
            var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
                + 'dd.descripcion as detalle_delito, titulo, fuente, '
                + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
                + 'p.descripcion AS profesion, tipo_arma,'
                + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
                + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
                + 'FROM sucesos AS s '
                + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
                + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
                + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
                + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
                + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
                + 'ORDER BY fecha_suceso DESC ';
            //console.log(query);
            Sucesos.query(query, function(err, sucesos) {
                if(err) console.log('error2'+err);
                _.forEach(sucesos, function(s) {
                    var day = moment(s.fecha_suceso,'YYYY-MM-DD');
                    arra1.push({'suceso_id' : s.suceso_id, 'fecha_suceso' : day.format("YYYY-MM-DD"), 'titulo' : s.titulo, 'nombre_victima' : s.nombre_victima});
                });
                callback();
            });

        },
        function(callback) {
            var query = 'SELECT '
                + 'SUM(CASE WHEN (YEAR(fecha_suceso)= YEAR(now()) ) THEN 1 ELSE 0 END) AS total, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso) = MONTH(now()) ) THEN 1 ELSE 0 END) AS tot_mes, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 1) THEN 1 ELSE 0 END) AS ene, '
    + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 2) THEN 1 ELSE 0 END) AS feb, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 3) THEN 1 ELSE 0 END) AS mar, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 4) THEN 1 ELSE 0 END) AS abr, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 5) THEN 1 ELSE 0 END) AS may, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 6) THEN 1 ELSE 0 END) AS jun, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 7) THEN 1 ELSE 0 END) AS jul, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 8) THEN 1 ELSE 0 END) AS ago, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 9) THEN 1 ELSE 0 END) AS sep, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 10) THEN 1 ELSE 0 END) AS oct, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 11) THEN 1 ELSE 0 END) AS nov, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 12) THEN 1 ELSE 0 END) AS dic '
                + 'FROM sucesos AS s '
                + 'WHERE YEAR(fecha_suceso) = YEAR(now()) '
                +  'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
            console.log(query);
            Sucesos.query(query, function(err, tot_homi_mes) {
                if(err) console.log('error1'+err);
                var fecha = moment().format('DD-MM-YYYY');
                _.forEach(tot_homi_mes, function(s) {
                    arra2.push({'total' : s.total,'tot_mes' : s.tot_mes,'ene' : s.ene,'feb' : s.feb,'mar' : s.mar,'abr' : s.abr,'may' : s.may,'jun' : s.jun,'jul' : s.jul,'ago' : s.ago,'sep' : s.sep,'oct' : s.oct,'nov' : s.nov,'dic' : s.dic});
                });
                callback();
            });

        },
    ],
        function(err, results) {
            //console.log(arra[0]);
            res.view({
            title: 'Venezuela Segura',
            title1: 'Sucesos',
            municipio: 'Caroni',
            estado: 'Bolivar',
            homi_mes_antes: arra,
            acumu_meses: arra2,
            sucesos: arra1

        });
    });

  },

  paginado: function (req, res) {
    var arra = [];
    var arra1 = [];
    var arra2 = [];
    var mes = moment().format('MM');

    async.parallel([
      function (callback) {
        var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant '
          + 'FROM sucesos AS s '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now()) AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        //Usado solo cuando es mes de Enero
        if (mes == '01') {
          var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant '
            + 'FROM sucesos AS s '
            + 'WHERE YEAR(fecha_suceso) = YEAR(now())-1 AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
            + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        };
        //console.log(query);
        Sucesos.query(query, function (err, tot_homi_mes) {
          if (err) console.log('error1' + err);
          _.forEach(tot_homi_mes, function (s) {
            arra.push({ 'homi_mes_ant': s.homi_mes_ant, 'ano': s.ano, 'mes': s.mes });
          });
          callback();
        });

      },
      function (callback) {
        var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
          + 'dd.descripcion as detalle_delito, titulo, fuente, '
          + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
          + 'p.descripcion AS profesion, tipo_arma,'
          + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
          + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
          + 'FROM sucesos AS s '
          + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
          + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
          + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
          + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
          + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
          + 'ORDER BY fecha_suceso DESC ';
        //console.log(query);
        Sucesos.query(query, function (err, sucesos) {
          if (err) console.log('error2' + err);
          _.forEach(sucesos, function (s) {
            var day = moment(s.fecha_suceso, 'YYYY-MM-DD');
            arra1.push({ 'suceso_id': s.suceso_id, 'fecha_suceso': day.format("YYYY-MM-DD"), 'titulo': s.titulo, 'nombre_victima': s.nombre_victima });
          });
          callback();
        });

      },
      function (callback) {
        var query = 'SELECT '
          + 'SUM(CASE WHEN (YEAR(fecha_suceso)= YEAR(now()) ) THEN 1 ELSE 0 END) AS total, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso) = MONTH(now()) ) THEN 1 ELSE 0 END) AS tot_mes, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 1) THEN 1 ELSE 0 END) AS ene, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 2) THEN 1 ELSE 0 END) AS feb, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 3) THEN 1 ELSE 0 END) AS mar, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 4) THEN 1 ELSE 0 END) AS abr, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 5) THEN 1 ELSE 0 END) AS may, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 6) THEN 1 ELSE 0 END) AS jun, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 7) THEN 1 ELSE 0 END) AS jul, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 8) THEN 1 ELSE 0 END) AS ago, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 9) THEN 1 ELSE 0 END) AS sep, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 10) THEN 1 ELSE 0 END) AS oct, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 11) THEN 1 ELSE 0 END) AS nov, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 12) THEN 1 ELSE 0 END) AS dic '
          + 'FROM sucesos AS s '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now()) '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        console.log(query);
        Sucesos.query(query, function (err, tot_homi_mes) {
          if (err) console.log('error1' + err);
          var fecha = moment().format('DD-MM-YYYY');
          _.forEach(tot_homi_mes, function (s) {
            arra2.push({ 'total': s.total, 'tot_mes': s.tot_mes, 'ene': s.ene, 'feb': s.feb, 'mar': s.mar, 'abr': s.abr, 'may': s.may, 'jun': s.jun, 'jul': s.jul, 'ago': s.ago, 'sep': s.sep, 'oct': s.oct, 'nov': s.nov, 'dic': s.dic });
          });
          callback();
        });

      },
    ],
    function (err, results) {
      //console.log(arra[0]);
      return res.json(200, {
        title: 'Venezuela Segura',
        title1: 'Sucesos',
        municipio: 'Caroni',
        estado: 'Bolivar',
        homi_mes_antes: arra,
        acumu_meses: arra2,
        sucesos: arra1

      })
    });

  },

  AcuAnoActual: function (req, res) {
    var arra = {};
    var arra2 = {};
    var mes = moment().format('MM');

    async.parallel([
      function (callback) {
        var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant '
          + 'FROM sucesos AS s '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now()) AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        //Usado solo cuando es mes de Enero
        if (mes == '01') {
          var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant '
            + 'FROM sucesos AS s '
            + 'WHERE YEAR(fecha_suceso) = YEAR(now())-1 AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
            + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        };
        //console.log(query);
        Sucesos.query(query, function (err, tot_homi_mes) {
          if (err) console.log('error1' + err);
          callback(null, tot_homi_mes);
        });

      },
      function (callback) {
        var query = 'SELECT '
          + 'SUM(CASE WHEN (YEAR(fecha_suceso)= YEAR(now()) ) THEN 1 ELSE 0 END) AS totalAcuAno, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso) = MONTH(now()) ) THEN 1 ELSE 0 END) AS tot_mes, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 1) THEN 1 ELSE 0 END) AS ene, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 2) THEN 1 ELSE 0 END) AS feb, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 3) THEN 1 ELSE 0 END) AS mar, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 4) THEN 1 ELSE 0 END) AS abr, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 5) THEN 1 ELSE 0 END) AS may, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 6) THEN 1 ELSE 0 END) AS jun, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 7) THEN 1 ELSE 0 END) AS jul, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 8) THEN 1 ELSE 0 END) AS ago, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 9) THEN 1 ELSE 0 END) AS sep, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 10) THEN 1 ELSE 0 END) AS oct, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 11) THEN 1 ELSE 0 END) AS nov, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 12) THEN 1 ELSE 0 END) AS dic '
          + 'FROM sucesos AS s '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now()) '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        //console.log(query);
        Sucesos.query(query, function (err, tot_homi_mes) {
          if (err) console.log('error1' + err);
          callback(null, tot_homi_mes);
        });

      },
    ],
      function (err, results) {
        //console.log(arra[0]);
        return res.json(200, {
          homi_mes_antes: results[0],
          acumu_meses: results[1],
          //homi_mes_antes:arra2
          //arra2
        })
      });

  },

  AcuAnoActual1: function (req, res) {
    var mes = moment().format('MM');
    async.parallel([
      function (callback) {
        var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant '
          + 'FROM sucesos AS s '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now()) AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        //Usado solo cuando es mes de Enero
        if (mes == '01') {
          var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant '
            + 'FROM sucesos AS s '
            + 'WHERE YEAR(fecha_suceso) = YEAR(now())-1 AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
            + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        };
        //console.log(query);
        Sucesos.query(query, function (err, tot_homi_mes_antes) {
          if (err) console.log('error1' + err);
          callback(null, tot_homi_mes_antes);
        });

      },
      function (callback) {
        var query = 'SELECT '
          + 'SUM(CASE WHEN (YEAR(fecha_suceso)= YEAR(now()) ) THEN 1 ELSE 0 END) AS totalAno, '
          + '(SELECT COUNT(fecha_suceso) AS homi_mes_ant  FROM sucesos AS su '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now())-1 '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + ') AS totalAnoAnt, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso) = MONTH(now()) ) THEN 1 ELSE 0 END) AS totalMes, '
          + '(SELECT COUNT(fecha_suceso) AS homi_mes_ant  FROM sucesos AS su '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now())-1 AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + ') AS totMesAntAnoAntes, '
          + '(SELECT COUNT(fecha_suceso) AS homi_mes_ant  FROM sucesos AS su '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now()) AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + ') AS totMesAnt, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 1) THEN 1 ELSE 0 END) AS ene, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 2) THEN 1 ELSE 0 END) AS feb, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 3) THEN 1 ELSE 0 END) AS mar, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 4) THEN 1 ELSE 0 END) AS abr, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 5) THEN 1 ELSE 0 END) AS may, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 6) THEN 1 ELSE 0 END) AS jun, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 7) THEN 1 ELSE 0 END) AS jul, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 8) THEN 1 ELSE 0 END) AS ago, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 9) THEN 1 ELSE 0 END) AS sep, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 10) THEN 1 ELSE 0 END) AS oct, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 11) THEN 1 ELSE 0 END) AS nov, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 12) THEN 1 ELSE 0 END) AS dic '
          + 'FROM sucesos AS s '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now()) '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        //console.log(query);
        Sucesos.query(query, function (err, tot_homi_mes) {
          if (err) console.log('error1' + err);
          callback(null, tot_homi_mes);
        });

      },
    ],
      function (err, results) {
        //console.log(arra[0]);
        return res.json(200,
          //tot_homi_mes
          //results[0],
          results[1]
          //acumu_meses: results[1],
        )
      });

  },

  AcuAnoActualParro1: function (req, res) {
    var arra = [];
    var arra2 = [];
    var mes = moment().format('MM');

    async.parallel([
      function (callback) {
        var query = 'SELECT '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso) = MONTH(now()) ) THEN 1 ELSE 0 END) AS tot_mes, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_cacha + ' ) THEN 1 ELSE 0 END) AS ano_parr_cacha, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_chi + ' ) THEN 1 ELSE 0 END) AS ano_parr_chi, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_dalla + ' ) THEN 1 ELSE 0 END) AS ano_parr_dalla, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_once + ' ) THEN 1 ELSE 0 END) AS ano_parr_once, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_pozo + ' ) THEN 1 ELSE 0 END) AS ano_parr_pozo, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_simon + ' ) THEN 1 ELSE 0 END) AS ano_parr_simon, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_unare + ' ) THEN 1 ELSE 0 END) AS ano_parr_unare, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_uni + ' ) THEN 1 ELSE 0 END) AS ano_parr_uni, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_vista + ' ) THEN 1 ELSE 0 END) AS ano_parr_vista, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_yoco + ' ) THEN 1 ELSE 0 END) AS ano_parr_yoco '
          + 'FROM sucesos AS s '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now()) AND MONTH(fecha_suceso) = MONTH(now()) '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        //console.log(query);
        //console.log(query);
        Sucesos.query(query, function (err, tot_homi_mesa) {
          if (err) console.log('error1' + err);
          callback(null, tot_homi_mesa);
        });

      },

      function (callback) {
        var query = 'SELECT '
          + 'SUM(CASE WHEN (YEAR(fecha_suceso)= YEAR(now()) ) THEN 1 ELSE 0 END) AS total, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso) = MONTH(now()) ) THEN 1 ELSE 0 END) AS tot_mes, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_cacha + ' ) THEN 1 ELSE 0 END) AS ano_parr_cacha, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_chi + ' ) THEN 1 ELSE 0 END) AS ano_parr_chi, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_dalla + ' ) THEN 1 ELSE 0 END) AS ano_parr_dalla, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_once + ' ) THEN 1 ELSE 0 END) AS ano_parr_once, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_pozo + ' ) THEN 1 ELSE 0 END) AS ano_parr_pozo, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_simon + ' ) THEN 1 ELSE 0 END) AS ano_parr_simon, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_unare + ' ) THEN 1 ELSE 0 END) AS ano_parr_unare, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_uni + ' ) THEN 1 ELSE 0 END) AS ano_parr_uni, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_vista + ' ) THEN 1 ELSE 0 END) AS ano_parr_vista, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_yoco + ' ) THEN 1 ELSE 0 END) AS ano_parr_yoco '
          + 'FROM sucesos AS s '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now()) '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        //console.log(query);
        //console.log(query);
        Sucesos.query(query, function (err, tot_homi_mes) {
          if (err) console.log('error1' + err);
          callback(null, tot_homi_mes);
        });

      },
    ],
      function (err, results) {
        //console.log(arra[0]);
        return res.json(200,
          //tot_homi_mes
          //results[0],
          results[1]
          //acumu_meses: results[1],
        )
      });

  },

  AcuAnoActualParro: function (req, res) {
    var arra = [];
    var arra2 = [];
    var mes = moment().format('MM');

    async.parallel([
      function (callback) {
        var query = 'SELECT '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso) = MONTH(now()) ) THEN 1 ELSE 0 END) AS tot_mes, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_cacha + ' ) THEN 1 ELSE 0 END) AS mes_parr_cacha, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_chi + ' ) THEN 1 ELSE 0 END) AS mes_parr_chi, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_dalla + ' ) THEN 1 ELSE 0 END) AS mes_parr_dalla, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_once + ' ) THEN 1 ELSE 0 END) AS mes_parr_once, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_pozo + ' ) THEN 1 ELSE 0 END) AS mes_parr_pozo, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_simon + ' ) THEN 1 ELSE 0 END) AS mes_parr_simon, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_unare + ' ) THEN 1 ELSE 0 END) AS mes_parr_unare, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_uni + ' ) THEN 1 ELSE 0 END) AS mes_parr_uni, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_vista + ' ) THEN 1 ELSE 0 END) AS mes_parr_vista, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_yoco + ' ) THEN 1 ELSE 0 END) AS mes_parr_yoco '
          + 'FROM sucesos AS s '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now()) AND MONTH(fecha_suceso) = MONTH(now()) '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        //console.log(query);
        Sucesos.query(query, function (err, tot_homi_mes) {
          if (err) console.log('error1' + err);
          var fecha = moment().format('DD-MM-YYYY');
          _.forEach(tot_homi_mes, function (s) {
            arra.push({ 'totMes': s.tot_mes, 'mes_parr_cacha': s.mes_parr_cacha, 'mes_parr_chi': s.mes_parr_chi, 'mes_parr_dalla': s.mes_parr_dalla, 'mes_parr_once': s.mes_parr_once, 'mes_parr_pozo': s.mes_parr_pozo, 'mes_parr_simon': s.mes_parr_simon, 'mes_parr_unare': s.mes_parr_unare, 'mes_parr_uni': s.mes_parr_uni, 'mes_parr_vista': s.mes_parr_vista, 'mes_parr_yoco': s.mes_parr_yoco, 'nov': s.nov, 'dic': s.dic });
          });
          callback();
        });

      },

      function (callback) {
        var query = 'SELECT '
          + 'SUM(CASE WHEN (YEAR(fecha_suceso)= YEAR(now()) ) THEN 1 ELSE 0 END) AS total, '
          + 'SUM(CASE WHEN (MONTH(fecha_suceso) = MONTH(now()) ) THEN 1 ELSE 0 END) AS tot_mes, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_cacha + ' ) THEN 1 ELSE 0 END) AS ano_parr_cacha, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_chi + ' ) THEN 1 ELSE 0 END) AS ano_parr_chi, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_dalla + ' ) THEN 1 ELSE 0 END) AS ano_parr_dalla, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_once + ' ) THEN 1 ELSE 0 END) AS ano_parr_once, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_pozo + ' ) THEN 1 ELSE 0 END) AS ano_parr_pozo, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_simon + ' ) THEN 1 ELSE 0 END) AS ano_parr_simon, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_unare + ' ) THEN 1 ELSE 0 END) AS ano_parr_unare, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_uni + ' ) THEN 1 ELSE 0 END) AS ano_parr_uni, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_vista + ' ) THEN 1 ELSE 0 END) AS ano_parr_vista, '
          + 'SUM(CASE WHEN (parroquia_id = ' + parro_yoco + ' ) THEN 1 ELSE 0 END) AS ano_parr_yoco '
          + 'FROM sucesos AS s '
          + 'WHERE YEAR(fecha_suceso) = YEAR(now()) '
          + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
        //console.log(query);
        Sucesos.query(query, function (err, tot_homi_mes) {
          if (err) console.log('error1' + err);
          var fecha = moment().format('DD-MM-YYYY');
          _.forEach(tot_homi_mes, function (s) {
            arra2.push({ 'totalAno': s.total, 'ano_parr_cacha': s.ano_parr_cacha, 'ano_parr_chi': s.ano_parr_chi, 'ano_parr_dalla': s.ano_parr_dalla, 'ano_parr_once': s.ano_parr_once, 'ano_parr_pozo': s.ano_parr_pozo, 'ano_parr_simon': s.ano_parr_simon, 'ano_parr_unare': s.ano_parr_unare, 'ano_parr_uni': s.ano_parr_uni, 'ano_parr_vista': s.ano_parr_vista, 'ano_parr_yoco': s.ano_parr_yoco, 'nov': s.nov, 'dic': s.dic });
          });
          callback();
        });

      },
    ],
      function (err, results) {
        //console.log(arra[0]);
        return res.json(200, {
          acu_mes: arra,
          acumu_ano: arra2
        })
      });

  },

  AcuAnoActualMesantes: function (req, res) {
    var mes = moment().format('MM');
    var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant '
      + 'FROM sucesos AS s '
      + 'WHERE YEAR(fecha_suceso) = YEAR(now()) AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
      + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
    //Usado solo cuando es mes de Enero
    if (mes == '01') {
      var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant '
        + 'FROM sucesos AS s '
        + 'WHERE YEAR(fecha_suceso) = YEAR(now())-1 AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
        + 'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
    };
    //console.log(query);
    //console.log(query);
    Sucesos.query(query, function (err, sucesos) {
      if (err) console.log(err);
      return res.json(200, sucesos);
    });

  },

  paginado1: function(req, res){
    var registros = [];
    var arra = [];
    var arra1 = [];
    var arra2 = [];
    var mes = moment().format('MM');  //si mes es 1 hay q hacer un if
    var mes_1 = moment().format('MM')-1;
    var ano = moment().format('YYYY');
    var ano_1 = moment().format('YYYY')-1;

    async.parallel([
        function(callback) {
            var query = 'SELECT COUNT(fecha_suceso) AS total '
                + 'FROM sucesos AS s '
                +  'WHERE delito_detalle_id = ' + delito_deta + '';

            console.log(query);
            Sucesos.query(query, function(err, tot_registros) {
                if(err) console.log('error1'+err);
                _.forEach(tot_registros, function(s) {
                    registros.push({'total' : s.total});
                });
                callback();
            });

        },
        function(callback) {
            var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant '
                + 'FROM sucesos AS s '
                + 'WHERE YEAR(fecha_suceso) = YEAR(now()) AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
                +  'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
            //Usado solo cuando es mes de Enero
            if (mes=='01') {
                var query = 'SELECT COUNT(fecha_suceso) AS homi_mes_ant '
                + 'FROM sucesos AS s '
                + 'WHERE YEAR(fecha_suceso) = YEAR(now())-1 AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) '
                +  'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
            };
            console.log(query);
            Sucesos.query(query, function(err, tot_homi_mes) {
                if(err) console.log('error1'+err);
                _.forEach(tot_homi_mes, function(s) {
                    arra.push({'homi_mes_ant' : s.homi_mes_ant,'ano' : ano,'mes' : mes_1});
                });
                callback();
            });

        },
        function(callback) {
            var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
                + 'dd.descripcion as detalle_delito, titulo, fuente, '
                + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
                + 'p.descripcion AS profesion, tipo_arma,'
                + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
                + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
                + 'FROM sucesos AS s '
                + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
                + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
                + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
                + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
                + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
                + 'ORDER BY fecha_suceso DESC LIMIT 8 ';
            //console.log(query);
            Sucesos.query(query, function(err, sucesos) {
                if(err) console.log('error2'+err);
                _.forEach(sucesos, function(s) {
                    var day = moment(s.fecha_suceso,'YYYY-MM-DD');
                    arra1.push({'suceso_id' : s.suceso_id, 'fecha_suceso' : s.fecha_suceso, 'titulo' : s.titulo, 'fuente' : s.fuente, 'nombre_victima' : s.nombre_victima});
                });
                callback();
            });

        },
        function(callback) {
            var query = 'SELECT '
                + 'SUM(CASE WHEN (YEAR(fecha_suceso)= YEAR(now()) ) THEN 1 ELSE 0 END) AS total, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso) = MONTH(now()) ) THEN 1 ELSE 0 END) AS tot_mes, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 1) THEN 1 ELSE 0 END) AS ene, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 2) THEN 1 ELSE 0 END) AS feb, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 3) THEN 1 ELSE 0 END) AS mar, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 4) THEN 1 ELSE 0 END) AS abr, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 5) THEN 1 ELSE 0 END) AS may, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 6) THEN 1 ELSE 0 END) AS jun, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 7) THEN 1 ELSE 0 END) AS jul, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 8) THEN 1 ELSE 0 END) AS ago, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 9) THEN 1 ELSE 0 END) AS sep, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 10) THEN 1 ELSE 0 END) AS oct, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 11) THEN 1 ELSE 0 END) AS nov, '
                + 'SUM(CASE WHEN (MONTH(fecha_suceso)= 12) THEN 1 ELSE 0 END) AS dic '
                + 'FROM sucesos AS s '
                + 'WHERE YEAR(fecha_suceso) = YEAR(now()) '
                +  'AND municipio_id =' + municipio_id + ' AND delito_detalle_id = ' + delito_deta + '';
            //console.log(query);
            Sucesos.query(query, function(err, tot_homi_mes) {
                if(err) console.log('error1'+err);
                var fecha = moment().format('DD-MM-YYYY');
                _.forEach(tot_homi_mes, function(s) {
                    arra2.push({'fecha' : fecha,'ano' : ano,'mes' : mes,'total' : s.total,'tot_mes' : s.tot_mes,'ene' : s.ene,'feb' : s.feb,'mar' : s.mar,'abr' : s.abr,'may' : s.may,'jun' : s.jun,'jul' : s.jul,'ago' : s.ago,'sep' : s.sep,'oct' : s.oct,'nov' : s.nov,'dic' : s.dic});
                });
                callback();
            });

        },
    ],
        function(err) {
        return res.json(200,{
            registros: registros,
            homi_mes_antes: arra,
            acumu_meses: arra2,
            sucesos: arra1
        })
    });

  },

  acuAnos: function (req, res) {
    var registros = [];
    var arra = [];
    var arra1 = [];
    var arra2 = [];
    var mes = moment().format("MM"); //si mes es 1 hay q hacer un if
    var mes_1 = moment().format("MM") - 1;
    var ano = moment().format("YYYY");
    var ano_1 = moment().format("YYYY") - 1;
    var ano_2 = moment().format("YYYY") - 2;
    var ano_3 = moment().format("YYYY") - 3;
    var ano_4 = moment().format("YYYY") - 4;
    //console.log(ano + ' ' + ano_1 + ' ' + ano_2 + ' ' + ano_3 + ' ' + ano_4);
    async.parallel(
      [
        function (callback) {
          var query =
            "SELECT COUNT(fecha_suceso) AS total " +
            "FROM sucesos AS s " +
            "WHERE delito_detalle_id = " + delito_deta + "";

          Sucesos.query(query, function (err, tot_registros) {
            if (err) console.log("error1" + err);
            callback(null, tot_registros);
          });
        },
        function (callback) {
          var query =
            "SELECT " + ano + "  AS ano, "  +
            "SUM(CASE WHEN (YEAR(fecha_suceso)= YEAR(now()) ) THEN 1 ELSE 0 END) AS total, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso) = MONTH(now()) ) THEN 1 ELSE 0 END) AS tot_mes, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 1) THEN 1 ELSE 0 END) AS ene, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 2) THEN 1 ELSE 0 END) AS feb, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 3) THEN 1 ELSE 0 END) AS mar, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 4) THEN 1 ELSE 0 END) AS abr, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 5) THEN 1 ELSE 0 END) AS may, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 6) THEN 1 ELSE 0 END) AS jun, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 7) THEN 1 ELSE 0 END) AS jul, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 8) THEN 1 ELSE 0 END) AS ago, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 9) THEN 1 ELSE 0 END) AS sep, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 10) THEN 1 ELSE 0 END) AS oct, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 11) THEN 1 ELSE 0 END) AS nov, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 12) THEN 1 ELSE 0 END) AS dic " +
            "FROM sucesos AS s " +
            "WHERE YEAR(fecha_suceso) = " + ano  +
            " AND municipio_id =" + municipio_id +
            " AND delito_detalle_id = " +
            delito_deta +
            "";
          //console.log(query);
          Sucesos.query(query, function(err, homiAnoMesesCaro) {
            if (err) console.log("error1" + err);
            callback(null, homiAnoMesesCaro);
          });
        },
        function (callback) {
          var query =
            "SELECT " + ano_1 + "  AS ano, "  +
            "SUM(CASE WHEN (YEAR(fecha_suceso)= " + ano_1 + "  ) THEN 1 ELSE 0 END) AS total, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 1) THEN 1 ELSE 0 END) AS ene, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 2) THEN 1 ELSE 0 END) AS feb, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 3) THEN 1 ELSE 0 END) AS mar, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 4) THEN 1 ELSE 0 END) AS abr, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 5) THEN 1 ELSE 0 END) AS may, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 6) THEN 1 ELSE 0 END) AS jun, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 7) THEN 1 ELSE 0 END) AS jul, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 8) THEN 1 ELSE 0 END) AS ago, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 9) THEN 1 ELSE 0 END) AS sep, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 10) THEN 1 ELSE 0 END) AS oct, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 11) THEN 1 ELSE 0 END) AS nov, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 12) THEN 1 ELSE 0 END) AS dic " +
            "FROM sucesos AS s " +
            "WHERE YEAR(fecha_suceso) = " + ano_1  +
            " AND municipio_id =" + municipio_id +
            " AND delito_detalle_id = " + delito_deta + "";
          //console.log(query);
          Sucesos.query(query, function(err, homiAno_1MesesCaro) {
            if (err) console.log("error1" + err);
            callback(null, homiAno_1MesesCaro);
          });
        },
        function (callback) {
          var query =
            "SELECT " + ano_2 + "  AS ano, "  +
            "SUM(CASE WHEN (YEAR(fecha_suceso)= " + ano_2 + "  ) THEN 1 ELSE 0 END) AS total, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 1) THEN 1 ELSE 0 END) AS ene, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 2) THEN 1 ELSE 0 END) AS feb, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 3) THEN 1 ELSE 0 END) AS mar, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 4) THEN 1 ELSE 0 END) AS abr, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 5) THEN 1 ELSE 0 END) AS may, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 6) THEN 1 ELSE 0 END) AS jun, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 7) THEN 1 ELSE 0 END) AS jul, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 8) THEN 1 ELSE 0 END) AS ago, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 9) THEN 1 ELSE 0 END) AS sep, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 10) THEN 1 ELSE 0 END) AS oct, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 11) THEN 1 ELSE 0 END) AS nov, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 12) THEN 1 ELSE 0 END) AS dic " +
            "FROM sucesos AS s " +
            "WHERE YEAR(fecha_suceso) = " + ano_2  +
            " AND municipio_id =" + municipio_id +
            " AND delito_detalle_id = " + delito_deta + "";
          //console.log(query);
          Sucesos.query(query, function(err, homiAno_2MesesCaro) {
            if (err) console.log("error1" + err);
            callback(null, homiAno_2MesesCaro);
          });
        },
        function (callback) {
          var query =
            "SELECT " + ano_3 + "  AS ano, "  +
            "SUM(CASE WHEN (YEAR(fecha_suceso)= " + ano_3 + "  ) THEN 1 ELSE 0 END) AS total, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 1) THEN 1 ELSE 0 END) AS ene, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 2) THEN 1 ELSE 0 END) AS feb, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 3) THEN 1 ELSE 0 END) AS mar, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 4) THEN 1 ELSE 0 END) AS abr, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 5) THEN 1 ELSE 0 END) AS may, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 6) THEN 1 ELSE 0 END) AS jun, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 7) THEN 1 ELSE 0 END) AS jul, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 8) THEN 1 ELSE 0 END) AS ago, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 9) THEN 1 ELSE 0 END) AS sep, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 10) THEN 1 ELSE 0 END) AS oct, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 11) THEN 1 ELSE 0 END) AS nov, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 12) THEN 1 ELSE 0 END) AS dic " +
            "FROM sucesos AS s " +
            "WHERE YEAR(fecha_suceso) = " + ano_3  +
            " AND municipio_id =" + municipio_id +
            " AND delito_detalle_id = " + delito_deta + "";
          //console.log(query);
          Sucesos.query(query, function(err, homiAno_3MesesCaro) {
            if (err) console.log("error1" + err);
            callback(null, homiAno_3MesesCaro);
          });
        },
      ],
      function (err,results) {
        return res.json(200, {
          registros: results[0],
          homiAnoMesesCaro: results[1],
          homiAno_1MesesCaro: results[2],
          homiAno_2MesesCaro: results[3],
          homiAno_3MesesCaro: results[4]
        });
      }
    );
  },

	get: function (req, res) {

		Sucesos.find().populate('delito_id').populate('delito_detalle_id').populate('parroquia_id').exec(function (err, response){
			if (err) return res.serverError(err);

			return res.json(200,response);
		});

	},

	get1: function (req, res) {
		var values = req.allParams();
		var suceso = values.suceso;
		//console.log('llego'+values);
		//if (req.isAjax) var suceso=455;

		var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
		+ 'dd.descripcion as detalle_delito, titulo, fuente, '
		+ 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
		+ 'p.descripcion AS profesion, tipo_arma,'
		+ 'm.descripcion as municipio, pa.descripcion AS parroquia, '
		+ 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
		+ 'FROM sucesos AS s '
		+ 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
		+ 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
		+ 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
		+ 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
		+ 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
		+ 'WHERE suceso_id =' + suceso + '';

		Sucesos.query(query, function(err, suceso) {

			if(err) return res.redirect('/');
			var array = [];

	        _.forEach(suceso, function(s) {

	        	var day = moment(s.fecha_suceso,'YYYY-MM-DD');
	        	array.push({'suceso_id' : s.suceso_id, 'titulo' : s.titulo,
	        		'dia' : day.format("DD-MM-YYYY"), 'hora' : s.hora, 'fuente' : s.fuente,
	        		'nombre_victima' : s.nombre_victima, 'edad' : s.edad, 'sexo' : s.sexo, 'detalle_delito' : s.detalle_delito, 'municipio' : s.municipio,
	        		'parroquia' : s.parroquia, 'mi_resena' : s.mi_resena});

	        });

			//console.log(array[0]);
			res.view('sucesos/suceso', {
		        title: 'Suceso',
		        layout: null,
		        suceso: array
		    });


		});

	},


	list: function (req, res) {

		Sucesos.find().exec(function (err, response){
			if (err) return res.serverError(err);

			return res.json(200,response);
		});

	},


	list1: function (req, res) {

		var values = req.allParams()

        //console.log('Skip '+values.skip+ ' Top '+values.top);
        var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
		+ 'dd.descripcion as detalle_delito, titulo, fuente, '
		+ 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
		+ 'p.descripcion AS profesion, tipo_arma,'
		+ 'm.descripcion as municipio, pa.descripcion AS parroquia, '
		+ 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
		+ 'FROM sucesos AS s '
		+ 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
		+ 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
		+ 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
		+ 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
		+ 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
		+ 'WHERE sexo ="' + values.sexo + '" AND tipo_arma ="' + values.tipo_arma + '" '
		+ 'AND p.descripcion ="' + values.profesion + '" '
		+ 'ORDER BY fecha_suceso DESC '
		+ 'LIMIT ' + values.skip + ',' + values.top;

		//console.log(query);
		Sucesos.query(query, function(err, sucesos) {
	    			if(err) console.log(err);
	    			return res.json(200,sucesos);
				});

	},

 	//por titulo2
	list2: function (req, res) {

		var values = req.allParams()

        //console.log('Skip '+values.skip+ ' Top '+values.top);
        var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
		+ 'dd.descripcion as detalle_delito, titulo, fuente, '
		+ 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
		+ 'p.descripcion AS profesion, tipo_arma,'
		+ 'm.descripcion as municipio, pa.descripcion AS parroquia, '
		+ 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
		+ 'FROM sucesos AS s '
		+ 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
		+ 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
		+ 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
		+ 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
		+ 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
		+ 'WHERE titulo LIKE "%' + values.titulo + '%" '
		+ 'ORDER BY fecha_suceso DESC '
		+ 'LIMIT ' + values.skip + ',' + values.top;

		console.log(query);
		Sucesos.query(query, function(err, sucesos) {
      if(err) console.log(err);
      return res.json(200,sucesos);
    });

	},

    //por municipio
    list22: function (req, res) {

        var values = req.allParams()

        //console.log('Skip '+values.skip+ ' Top '+values.top);
        var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
        + 'dd.descripcion as detalle_delito, titulo, fuente, '
        + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
        + 'p.descripcion AS profesion, tipo_arma,'
        + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
        + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
        + 'FROM sucesos AS s '
        + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
        + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
        + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
        + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
        + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
        + 'WHERE s.municipio_id LIKE "%' + values.municipioid + '%" '
        + 'ORDER BY fecha_suceso DESC '
        + 'LIMIT ' + values.skip + ',' + values.top;

        console.log(query);
        Sucesos.query(query, function(err, sucesos) {
                    if(err) console.log(err);
                    return res.json(200,sucesos);
                });

    },

  //por titulo2
  list3: function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");

    var values = req.allParams();
    var page_after = parseInt(values.page) + 1;
    var page_before = parseInt(values.page) - 1;
    //console.log('Skip '+values.skip+ ' Top '+values.top);
    var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
      + 'dd.descripcion as detalle_delito, titulo, fuente, '
      + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
      + 'p.descripcion AS profesion, tipo_arma,'
      + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
      + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena, '
      + 'CONCAT("http://34.229.201.249/img/", suceso_id, ".jpg" ) as url_img, '
      + values.page + ' AS page, ' + page_after + ' AS page_after '
      + 'FROM sucesos AS s '
      + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
      + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
      + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
      + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
      + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
      + 'ORDER BY fecha_suceso DESC '
      + 'LIMIT ' + values.page + ',' + values.per_page;

    //console.log(query);
    Sucesos.query(query, function (err, sucesos) {
      if (err) console.log(err);
      return res.json(200, sucesos);
    });

  },

  //por titulo2
  list33: function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");

    var values = req.allParams();
    var page_after = parseInt(values.page) + 1;
    var page_before = parseInt(values.page) - 1;


    //console.log('Skip '+values.skip+ ' Top '+values.top);
    var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
      + 'dd.descripcion as detalle_delito, titulo, fuente, '
      + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
      + 'p.descripcion AS profesion, tipo_arma,'
      + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
      + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena, '
      + 'CONCAT("http://34.229.201.249/img/", suceso_id, ".jpg" ) as url_img, '
      + values.page + ' AS page, ' + page_after + ' AS page_after '
      + 'FROM sucesos AS s '
      + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
      + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
      + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
      + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
      + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
      + 'ORDER BY fecha_suceso DESC '
      + 'LIMIT ' + values.page + ',' + values.results;

    //console.log(query);
    Sucesos.query(query, function (err, sucesos) {
      if (err) console.log(err);

      return res.json(200, {
        results: sucesos,
        page: values.page,
        resultss: values.results,
      })
    });
  },

    list4: function (req, res) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");

        var values = req.allParams();
        //console.log('Skip '+values.skip+ ' Top '+values.top);
        var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
            + 'dd.descripcion as detalle_delito, titulo, fuente, '
            + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
            + 'p.descripcion AS profesion, tipo_arma,'
            + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
            + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
            + 'FROM sucesos AS s '
            + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
            + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
            + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
            + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
            + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
            + 'ORDER BY fecha_suceso DESC '
            + 'LIMIT ' + values.page + ',' + values.per_page;
        var queryCount = 'SELECT COUNT(*) AS results FROM sucesos';

        async.parallel([
            function (callback) {
                Sucesos.query(queryCount, function (err, sucesosCount) {
                    if (err)
                        console.log(err);
                    callback(null, sucesosCount);
                });
            },
            function (callback) {
                Sucesos.query(query, function (err, sucesos) {
                    if (err)
                        console.log(err);
                    callback(null, sucesos);
                });
            },
        ], function (err, results) {
            return res.json(200, {
                //results: results[0],
                //tutors: results[1]
                results: results[1]

            });
        });

    },

  list44: function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");

    var values = req.allParams();
    var arra = [];
    var datas = [];
    var total = 0;
    //console.log('Skip '+values.skip+ ' Top '+values.top);
    var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
      + 'dd.descripcion as detalle_delito, titulo, fuente, '
      + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
      + 'p.descripcion AS profesion, tipo_arma,'
      + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
      + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
      + 'FROM sucesos AS s '
      + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
      + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
      + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
      + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
      + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
      + 'ORDER BY fecha_suceso DESC '
      + 'LIMIT ' + values.page + ',' + values.results;
    var queryCount = 'SELECT COUNT(*) AS totalregistros FROM sucesos';

    async.parallel([

      function (callback) {
        Sucesos.query(query, function (err, ssucesos) {
          if (err) console.log('error2' + err);
          _.forEach(ssucesos, function (s) {
            datas.push({ 'suceso_id': s.suceso_id, 'fecha_suceso': s.fecha_suceso, 'hora': s.hora, 'titulo': s.titulo, 'fuente': s.fuente, 'nombre_victima': s.nombre_victima, 'edad': s.edad, 'resena': s.mi_resena, 'municipio_id': s.municipio_id, 'municipio': s.municipio });
          });
          callback();
        });
      },
      function (callback) {
        Sucesos.query(queryCount, function (err, sucesosCount) {
          if (err) console.log(err);
          _.forEach(sucesosCount, function (s) {
            totalregistros = s.totalregistros
            arra.push({ 'seed': values.page });
            arra.push({ 'results': values.results });
            arra.push({ 'page': values.page });
            arra.push({ 'totaldata': totalregistros });
            arra.push({ 'version': "1.2" });
          });
          callback();
        });
      },
    ],
      function (err, results) {
        return res.json(200, {
          //results: results[0],
          //tutors: results[1]
          results: datas,
          info: arra,

        });
      });
  },


	sql: function (req, res) {

		var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
		+ 'dd.descripcion as detalle_delito, titulo, fuente, '
		+ 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
		+ 'p.descripcion AS profesion, tipo_arma,'
		+ 'm.descripcion as municipio, pa.descripcion AS parroquia, '
		+ 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
		+ 'FROM sucesos AS s '
		+ 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
		+ 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
		+ 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
		+ 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
		+ 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
		+ 'ORDER BY fecha_suceso DESC LIMIT 8';
		//console.log(query);
		Sucesos.query(query, function(err, sucesos) {
	    			if(err) console.log(err);
	    			return res.json(200,sucesos);
				});
	},

  find: function (req, res) {

    var values = req.allParams();
    //console.log('suceso '+values.suceso_id);

    Sucesos.findOne({suceso_id: values.suceso_id}).exec(function (err, response){
      if (err) return res.serverError(err);

      return res.json(200,response);
    });

	},

	find1: function (req, res) {

  		var values = req.allParams();
  		//console.log('suceso '+values.suceso_id);

		Sucesos.findOne({suceso_id: values.suceso_id}).populate('delito_id').populate('delito_detalle_id').exec(function (err, response){
			if (err) return res.serverError(err);

			return res.json(200,response);
		});

	},

	findsql: function (req, res) {
    var values = req.allParams();
    var query = 'SELECT suceso_id, fecha_suceso, hora, s.delito_id, d.descripcion as tipo_delito, '
      + 's.delito_detalle_id, dd.descripcion as detalle_delito, titulo, fuente, '
      + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
      + 'p.descripcion AS profesion, tipo_arma,'
      + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
      + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena, '
      + 'CONCAT("http://localhost/venezuelasegura_front/img/", suceso_id, ".jpg" ) as url_img, fecha_creado '
      + 'FROM sucesos AS s '
      + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
      + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
      + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
      + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
      + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
      + 'WHERE suceso_id ="' + values.suceso_id + '"';
  		//console.log('suceso '+values.suceso_id);
		Sucesos.query(query, function(err, suceso) {
			if (err) return res.serverError(err);
			return res.json(200,suceso[0]);   //Devuelve uno solo  no []
		});
  },

  findAll: function (req, res) {
    var values = req.allParams();
    var search = values.search;
    var years = "1900-01-01";
    var searchDate = "(YEAR(fecha_suceso) = YEAR('" + years + "'))";

    var expresion = /^([0][1-9]|[12][0-9]|3[01])(\/|-)([0][1-9]|[1][0-2])\2(\d{4})$/gm;  //01-05-2018
    var typeSearch = search.match(expresion);
    if (typeSearch) {     //Si es fecha 01-05-2018
      var typeDate = search.substring(6, 10) + "-" + search.substring(3, 5) + "-" + search.substring(0, 2);  //2018-05-01
      var searchDate = "(YEAR(fecha_suceso) = YEAR('" + typeDate + "') AND MONTH(fecha_suceso) = MONTH('" + typeDate + "') AND DAY(fecha_suceso) = DAY('" + typeDate + "'))";
    }

    var expresionM = /^([0][1-9]|[1][0-2])(\/|-)(\d{4})$/gm;  //para  mes 05-2018
    var typeSearchM = search.match(expresionM);
    if (typeSearchM) {  //Si es fecha 05-2018
      var typeDate = search.substring(3, 7) + "-" + search.substring(0, 2) + "-01";  //2018-05-01
      var searchDate = "(YEAR(fecha_suceso) = YEAR('" + typeDate + "') AND MONTH(fecha_suceso) = MONTH('" + typeDate + "') )";
    }
    var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
      + 'dd.descripcion as detalle_delito, titulo, fuente, '
      + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
      + 'p.descripcion AS profesion, tipo_arma,'
      + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
      + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
      + 'FROM sucesos AS s '
      + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
      + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
      + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
      + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
      + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
      + 'WHERE suceso_id LIKE "' + values.search + '"'
      + ' OR nombre_victima LIKE "%' + values.search + '%"'
      + ' OR titulo LIKE "%' + values.search + '%"'
      + ' OR sector LIKE "%' + values.search + '%"'
      + ' OR p.descripcion LIKE "%' + values.search + '%"'
      + ' OR mi_resena LIKE "%' + values.search + '%"'
      + ' OR ' + searchDate + ''
      + ' ORDER BY fecha_suceso DESC';
    //console.log(query);
    Sucesos.query(query, function (err, suceso) {
      if (err) return res.serverError(err);
      return res.json(200, suceso);   //Devuelve uno solo  no []
    });
  },

  findAll8: function (req, res) {
    var values = req.allParams();
    var search = values.search;
    var years = "1900-01-01";
    var searchDate = "(YEAR(fecha_suceso) = YEAR('" + years + "'))";

    var expresion = /^([0][1-9]|[12][0-9]|3[01])(\/|-)([0][1-9]|[1][0-2])\2(\d{4})$/gm;  //01-05-2018
    var typeSearch = search.match(expresion);
    if (typeSearch) {     //Si es fecha 01-05-2018
      var typeDate = search.substring(6, 10) + "-" + search.substring(3, 5) + "-" + search.substring(0, 2);  //2018-05-01
      var searchDate = "(YEAR(fecha_suceso) = YEAR('" + typeDate + "') AND MONTH(fecha_suceso) = MONTH('" + typeDate + "') AND DAY(fecha_suceso) = DAY('" + typeDate + "'))";
    }

    var expresionM = /^([0][1-9]|[1][0-2])(\/|-)(\d{4})$/gm;  //para  mes 05-2018
    var typeSearchM = search.match(expresionM);
    if (typeSearchM) {  //Si es fecha 05-2018
      var typeDate = search.substring(3, 7) + "-" + search.substring(0, 2) + "-01";  //2018-05-01
      var searchDate = "(YEAR(fecha_suceso) = YEAR('" + typeDate + "') AND MONTH(fecha_suceso) = MONTH('" + typeDate + "') )";
    }

    var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
      + 'dd.descripcion as detalle_delito, titulo, fuente, '
      + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
      + 'p.descripcion AS profesion, tipo_arma,'
      + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
      + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena, '
      + 'CONCAT("http://34.229.201.249/img/", suceso_id, ".jpg" ) as url_img '
      + 'FROM sucesos AS s '
      + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
      + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
      + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
      + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
      + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
      + 'WHERE suceso_id LIKE "' + search + '"'
      + ' OR nombre_victima LIKE "%' + search + '%"'
      + ' OR titulo LIKE "%' + search + '%"'
      + ' OR sector LIKE "%' + search + '%"'
      + ' OR p.descripcion LIKE "%' + search + '%"'
      + ' OR mi_resena LIKE "%' + search + '%"'
      + ' OR ' + searchDate + ''
      + ' ORDER BY fecha_suceso DESC LIMIT 8';
    //console.log(query);
    Sucesos.query(query, function (err, suceso) {
      if (err) return res.serverError(err);
      return res.json(200, suceso);   //
    });
  },

  findAll8_: function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    var values = req.allParams();
    var query = values.query;
    var years = "1900-01-01";
    var searchDate = "(YEAR(fecha_suceso) = YEAR('" + years + "'))";

    var expresion = /^([0][1-9]|[12][0-9]|3[01])(\/|-)([0][1-9]|[1][0-2])\2(\d{4})$/gm; //01-05-2018
    var typeSearch = query.match(expresion);
    if (typeSearch) {
      //Si es fecha 01-05-2018
      var typeDate =
        query.substring(6, 10) +
        "-" +
        query.substring(3, 5) +
        "-" +
        query.substring(0, 2); //2018-05-01
      var searchDate =
        "(YEAR(fecha_suceso) = YEAR('" +
        typeDate +
        "') AND MONTH(fecha_suceso) = MONTH('" +
        typeDate +
        "') AND DAY(fecha_suceso) = DAY('" +
        typeDate +
        "'))";
    }

    var expresionM = /^([0][1-9]|[1][0-2])(\/|-)(\d{4})$/gm; //para  mes 05-2018
    var typeSearchM = query.match(expresionM);
    if (typeSearchM) {
      //Si es fecha 05-2018
      var typeDate =
        query.substring(3, 7) + "-" + query.substring(0, 2) + "-01"; //2018-05-01
      var queryDate =
        "(YEAR(fecha_suceso) = YEAR('" +
        typeDate +
        "') AND MONTH(fecha_suceso) = MONTH('" +
        typeDate +
        "') )";
    }

    var query =
      "SELECT suceso_id, DATE_FORMAT(fecha_suceso, '%d-%m-%Y') AS fecha_sucesos, hora, d.descripcion as tipo_delito, " +
      "dd.descripcion as detalle_delito, titulo, fuente, " +
      "otra_fuente1, otra_fuente2, nombre_victima, sexo, edad," +
      "p.descripcion AS profesion, tipo_arma," +
      "m.descripcion as municipio, pa.descripcion AS parroquia, " +
      "sector, s.latitud as latitud, s.longitud AS longitud, mi_resena, " +
      'CONCAT("http://18.216.139.132/vs/img/", suceso_id, ".jpg" ) as url_img ' +
      "FROM sucesos AS s " +
      "INNER JOIN delitos AS d ON s.delito_id = d.delito_id " +
      "INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id " +
      "INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id " +
      "INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id " +
      "INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id " +
      'WHERE suceso_id LIKE "' +
      query +
      '"' +
      ' OR nombre_victima LIKE "%' +
      query +
      '%"' +
      ' OR titulo LIKE "%' +
      query +
      '%"' +
      ' OR sector LIKE "%' +
      query +
      '%"' +
      ' OR p.descripcion LIKE "%' +
      query +
      '%"' +
      ' OR mi_resena LIKE "%' +
      query +
      '%"' +
      " OR " +
      searchDate +
      "" +
      " ORDER BY fecha_suceso DESC LIMIT 30";
    console.log(query);

    Sucesos.query(query, function (err, suceso) {
      if (err) return res.serverError(err);
      return res.json({ sucesos: suceso }); //
      //return res.json(200, suceso);   //Devuelve uno solo  no []
    });
  },


	findHomiMunicipios: function (req, res) {
    var values = req.allParams();
    var ano = moment().format('YYYY');
    var delitodetalleid = 7;
    var query = 'SELECT descripcion AS Municipio, count(*) As Homicidios '
		+ 'FROM sucesos AS s '
		+ 'INNER JOIN municipios AS m ON  s.municipio_id= m.municipio_id '
		+ 'WHERE year(`fecha_suceso`) =' + ano + ' AND delito_detalle_id =' + delitodetalleid + ' '
		+ 'GROUP BY descripcion';
		console.log('query '+query);
		Sucesos.query(query, function(err, sucesos) {
			if (err) return res.serverError(err);
			return res.json(200,sucesos);
		});
	},

	findHomiMunicipioId: function (req, res) {
    var values = req.allParams();
    var ano = moment().format('YYYY');
    var delitodetalleid = 7;
    var municipio_id = 3;

    var query = 'SELECT descripcion AS Municipio, count(*) As Homicidios '
		+ 'FROM sucesos AS s '
		+ 'INNER JOIN municipios AS m ON  s.municipio_id= m.municipio_id '
		+ 'WHERE year(`fecha_suceso`) =' + ano + ' AND delito_detalle_id =' + delitodetalleid + ' AND '
		+ 's.municipio_id="' + values.municipio_id + '" '
		+ 'GROUP BY descripcion';
		Sucesos.query(query, function(err, sucesos) {
			if (err) return res.serverError(err);
			return res.json(200,sucesos[0]); // devuelve uno solo  sin []  en el array
		});
	},

  //por titulo2
  cuantos_dame: function (req, res) {

      var values = req.allParams()

      var query = 'SELECT COUNT(*) total FROM sucesos';
      Sucesos.query(query, function(err, sucesos) {
          if(err) console.log(err);
          return res.json(200,sucesos);
      });

  },

  sql_: function (req, res) {

    var values = req.allParams();

    var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
    + 'dd.descripcion as detalle_delito, titulo, fuente, '
    + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
    + 'p.descripcion AS profesion, tipo_arma,'
    + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
    + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
    + 'FROM sucesos AS s '
    + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
    + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
    + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
    + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
    + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
    + 'ORDER BY fecha_suceso DESC LIMIT '+ values.offset + ',' + values.limit;
    console.log(query);
    Sucesos.query(query, function(err, sucesos) {
        if(err) console.log(err);
        return res.json(200,sucesos);
    });
  },


  paginas: function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
      //sort=fecha_suceso%7Casc&page=1&per_page=5
    var datas = [];
    var arra = [];
      var values = req.allParams();
      var total = 0;
      var sort = values.sort || '';
      var page = values.page || '';
      var per_page = values.per_page || '';

      //var nombre = req.query.nombre || '';
      if (page === '') {
          page = 1;
      };

      if (per_page === '') {
          per_page = 10;
      };

      page = parseInt(page)-1;
      nextpage = page+1;
      var per_page = parseInt(per_page);
      var from = page*per_page;
      var to = from +per_page;

      var sortAux = sort.split("|"); // | valor por el URL ?? https://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java
      //sails.log.debug(sortAux);

      if(sortAux.length > 1) {

          sort = sortAux[0]+" "+sortAux[1];
      }
      //sails.log.warn(sort);
      if (sort === '') {
          sort = "fecha_suceso DESC";
      };

      //console.log("per page "+per_page+ " page "+page + " from "+from + " to "+to)
      //var per_page = values.per_page;    sort=&page=1&per_page=10
      var last_page = 0;
      //var order = values.sort;
      //var order = "fecha_suceso DESC";
      var next_page_url = "https:\/\/192.168.0.106:1337\/api\/sucesos\/paginas?page="+nextpage;
      //values.page

      async.parallel([
        function(callback) {
          var query = 'SELECT COUNT(fecha_suceso) AS total '
              + 'FROM sucesos AS s '
              +  'WHERE delito_detalle_id = ' + delito_deta + '';

          //console.log(query);
          Sucesos.query(query, function(err, tot_registros) {
              if(err) console.log('error1'+err);
              _.forEach(tot_registros, function(s) {
                total = s.total
                arra.push({'total' : s.total});
                arra.push({'per_page' : per_page});
                arra.push({'current_page' : page});
                last_page = Math.ceil(s.total/per_page);
                arra.push({'last_page' : Math.ceil(s.total/per_page)});
                arra.push({'next_page_url' : next_page_url});
              });
              callback();
          });

        },
        function(callback) {
          var query = 'SELECT suceso_id, fecha_suceso, hora, d.descripcion as tipo_delito, '
              + 'dd.descripcion as detalle_delito, titulo, fuente, '
              + 'otra_fuente1, otra_fuente2, nombre_victima, sexo, edad,'
              + 'p.descripcion AS profesion, tipo_arma, m.municipio_id AS municipio_id,'
              + 'm.descripcion as municipio, pa.descripcion AS parroquia, '
              + 'sector, s.latitud as latitud, s.longitud AS longitud, mi_resena '
              + 'FROM sucesos AS s '
              + 'INNER JOIN delitos AS d ON s.delito_id = d.delito_id '
              + 'INNER JOIN delitos_detalles AS dd ON s.delito_detalle_id = dd.delito_detalle_id '
              + 'INNER JOIN profesiones AS p ON s.profesion_id = p.profesion_id '
              + 'INNER JOIN municipios AS m ON s.municipio_id = m.municipio_id '
              + 'INNER JOIN parroquias AS pa ON s.parroquia_id = pa.parroquia_id '
              + 'ORDER BY ' + sort + ' '
              + 'LIMIT ' + per_page + ' OFFSET ' + from;
          //console.log(query+' sort '+sort);
          Sucesos.query(query, function(err, ssucesos) {
              if(err) console.log('error2'+err);
              _.forEach(ssucesos, function(s) {
                  datas.push({'suceso_id' : s.suceso_id, 'fecha_suceso' : s.fecha_suceso, 'hora' : s.hora, 'titulo' : s.titulo, 'fuente' : s.fuente, 'nombre_victima' : s.nombre_victima, 'edad' : s.edad, 'resena' : s.mi_resena, 'municipio_id' : s.municipio_id, 'municipio' : s.municipio});
              });
              callback();
          });
        },
      ],
        function(err) {
        return res.json(200,{
          "total":total,
          "per_page":per_page,
          "current_page":page,
          "last_page":last_page,
          "next_page_url":next_page_url,
          "from":from,
          "to":to,
          data: datas
        })
    });

  },

  homianoparrocar: function (req, res) {
    var values = req.allParams()
    var query = 'SELECT s.parroquia_id As id, p.descripcion AS stationName, p.latitud AS latitude, p.longitud AS longitude, COUNT(*) AS total '
      + 'FROM sucesos AS s '
      + 'INNER JOIN parroquias AS p ON s.parroquia_id = p.parroquia_id '
      + 'WHERE s.municipio_id = ' + municipio_id + '  AND delito_detalle_id = ' + delito_deta + ' AND YEAR(fecha_suceso) = 2019 '
      + 'GROUP BY s.parroquia_id ';
    console.log(query);
    Sucesos.query(query, function (err, sucesos) {
      if (err) console.log(err);
      return res.json(200, {"stationBeanList":sucesos});
    });

  },


  create: function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    console.log('values',req.allParams());
    try {
      const validate = require("validate.js");
      const values = req.allParams();

      const constraints = {
        fecha_suceso: {
          presence: {
            message: "The fecha suceso is required"
          }
        },
        hora: {
          presence: {
            message: "The hora is required"
          }
        },
        delito_id: {
          presence: {
            message: "The delito_id is required"
          }
        },
        delito_detalle_id: {
          presence: {
            message: "The delito_detalle_id is required"
          }
        },
        titulo: {
          presence: {
            message: "The titulo is required"
          }
        },
        fuente: {
          presence: {
            message: "The fuente is required"
          }
        },
        estado: {
          presence: {
            message: "The estado is required"
          }
        },
        municipio_id: {
          presence: {
            message: "The municipio_id is required"
          }
        },
        parroquia_id: {
          presence: {
            message: "The parroquia_id is required"
          }
        },
        nombre_victima: {
          presence: {
            message: "The nombre_victima is required"
          }
        },
        fecha_creado: {
          presence: {
            message: "The fecha creado is required"
          }
        }
      };

      const validacion = validate(values, constraints, {
        format: "flat",
        fullMessages: false
      });

      if (validacion != null) {
        return res.json(400, {
          error: 'data_missing',
          error_description: validacion[0]
        });
      }


      Sucesos.create(values).exec(function (err, suceso) {
        if (err) {
          if (err.invalidAttributes.PRIMARY) {
            return res.json(400, {
              error: 'Suceso_already_exists',
              error_description: err
            });
          }
          return res.json(400, {
            error: 'data_missingg',
            error_description: err
          });
        }
        return res.json(suceso);
      });
    } catch (e) {
      if (e.invalidAttributes) return res.badRequest(e.Errors);
      return res.serverError(e);
    }
  },

  deletesuceso(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    const values = req.allParams();
    Sucesos.findOne({ suceso_id: values.suceso_id }).exec(function (err, suceso) {
      if (!suceso) {
        return res.json(400, {
          error: 'suceso_id_missing',
          error_description: 'The suceso does not exist'
        });
      }
      Sucesos.destroy({ suceso_id: values.suceso_id }).exec(function (err) {
          return res.json(200, {
            message: 'Suceso eliminated'
          });
      });
    });
  },

  update: function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    var values = req.allParams();
    console.log('suceso ' + values.suceso_id);
    if (!values.suceso_id || !values.titulo) {
      return res.json(400, {
        error: 'data_missing',
        error_description: 'The suceso_id and titulo is required',
      });
    }
    Sucesos.update({ suceso_id: values.suceso_id }, { titulo: values.titulo }).exec(function (err, response) {
      if (err) return res.serverError(err);
      return res.json(200, response);
    });

  },
  updatesuceso: function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");

    const validate = require("validate.js");
    const values = req.allParams();
    console.log('valuesUpdate', req.allParams());

    const constraints = {
      fecha_suceso: {
        presence: {
          message: "The fecha suceso is required"
        }
      },
      hora: {
        presence: {
          message: "The hora is required"
        }
      },
      delito_id: {
        presence: {
          message: "The delito_id is required"
        }
      },
      delito_detalle_id: {
        presence: {
          message: "The delito_detalle_id is required"
        }
      },
      titulo: {
        presence: {
          message: "The titulo is required"
        }
      },
      fuente: {
        presence: {
          message: "The fuente is required"
        }
      },
      estado: {
        presence: {
          message: "The estado is required"
        }
      },
      municipio_id: {
        presence: {
          message: "The municipio_id is required"
        }
      },
      parroquia_id: {
        presence: {
          message: "The parroquia_id is required"
        }
      },
      nombre_victima: {
        presence: {
          message: "The nombre_victima is required"
        }
      },
      fecha_creado: {
        presence: {
          message: "The fecha creado is required"
        }
      }
    };

    const validacion = validate(values, constraints, {
      format: "flat",
      fullMessages: false
    });

    if (validacion != null) {
      return res.json(400, {
        error: 'data_missing',
        error_description: validacion[0]
      });
    }

    console.log('suceso updated' + values.suceso_id);
    if (!values.suceso_id || !values.titulo) {
      return res.json(400, {
        error: 'data_missing',
        error_description: 'The suceso_id and titulo is required',
      });
    }
    Sucesos.update({ suceso_id: values.suceso_id }, { fecha_suceso: values.fecha_suceso, titulo: values.titulo, hora: values.hora, nombre_victima: values.nombre_victima }).exec(function (err, response) {
      if (err) return res.serverError(err);
      return res.json(200, response);
    });

  },

  getall: function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    var values = req.allParams();
    var query =
      "SELECT suceso_id, DATE_FORMAT(fecha_suceso, '%d-%m-%Y') AS fecha_suceso, titulo, fuente, " +
      'CONCAT("http://18.216.139.132/vs/img/", suceso_id, ".jpg" ) as url_img ' +
      "FROM sucesos AS s " +
      "ORDER BY fecha_suceso DESC";
    console.log(query);
    Sucesos.query(query, function (err, sucesos) {
      if (err) console.log(err);
      return res.json(200, sucesos);
    });
  },

  AcuAnoActual2: function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    res.header(
      "Access-Control-Allow-Methods",
      "PUT, GET, POST, DELETE, OPTIONS"
    );
    var mes = moment().format("MM");
    console.log('acuanoactual',)
    async.parallel(
      [
        function (callback) {
          var query =
            "SELECT COUNT(fecha_suceso) AS homi_mes_ant " +
            "FROM sucesos AS s " +
            "WHERE YEAR(fecha_suceso) = YEAR(now()) AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) " +
            "AND municipio_id =" +
            municipio_id +
            " AND delito_detalle_id = " +
            delito_deta +
            "";
          //Usado solo cuando es mes de Enero
          if (mes == "01") {
            var query =
              "SELECT COUNT(fecha_suceso) AS homi_mes_ant " +
              "FROM sucesos AS s " +
              "WHERE YEAR(fecha_suceso) = YEAR(now())-1 AND MONTH(fecha_suceso) = MONTH(DATE_ADD(CURDATE(),INTERVAL -1 MONTH)) " +
              "AND municipio_id =" +
              municipio_id +
              " AND delito_detalle_id = " +
              delito_deta +
              "";
          }
          //console.log(query);
          Sucesos.query(query, function (err, tot_homi_mes) {
            if (err) console.log("error1" + err);
            callback(null, tot_homi_mes);
          });
        },
        function (callback) {
          var query =
            "SELECT " +
            "SUM(CASE WHEN (YEAR(fecha_suceso)= YEAR(now()) ) THEN 1 ELSE 0 END) AS totalAcuAno, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso) = MONTH(now()) ) THEN 1 ELSE 0 END) AS tot_mes, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 1) THEN 1 ELSE 0 END) AS ene, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 2) THEN 1 ELSE 0 END) AS feb, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 3) THEN 1 ELSE 0 END) AS mar, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 4) THEN 1 ELSE 0 END) AS abr, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 5) THEN 1 ELSE 0 END) AS may, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 6) THEN 1 ELSE 0 END) AS jun, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 7) THEN 1 ELSE 0 END) AS jul, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 8) THEN 1 ELSE 0 END) AS ago, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 9) THEN 1 ELSE 0 END) AS sep, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 10) THEN 1 ELSE 0 END) AS oct, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 11) THEN 1 ELSE 0 END) AS nov, " +
            "SUM(CASE WHEN (MONTH(fecha_suceso)= 12) THEN 1 ELSE 0 END) AS dic " +
            "FROM sucesos AS s " +
            "WHERE YEAR(fecha_suceso) = YEAR(now()) " +
            "AND municipio_id =" +
            municipio_id +
            " AND delito_detalle_id = " +
            delito_deta +
            "";
          //console.log(query);
          Sucesos.query(query, function (err, tot_homi_mes) {
            if (err) console.log("error1" + err);
            callback(null, tot_homi_mes);
          });
        }
      ],
      function (err, results) {
        //console.log(arra[0]);
        return res.json(results[1].rows);
      }
    );
  },



};
