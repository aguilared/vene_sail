/**
 * ProvincesController
 *
 * @description :: Server-side logic for managing studentlevel
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	get: function (req, res) {

		var values = req.allParams();
  		Provinces.find(function (err, provinces){
	  		if (err) return res.serverError(err);
	  		return res.json(200,provinces);
		});

  	},

  	create: function (req, res) {

  		var values = req.allParams();

  		if(!values.provinceName){
  			return res.json(400, {
	      		error: 'data_missing',
				error_description: 'The provinceName is required',
	      	});
  		}
  		Provinces.create({provinceName : values.provinceName}).exec(function (err, province){
	  		if (err) return res.serverError(err);
	  		return res.json(200,province);
		});

  	},

  	update: function (req, res) {

  		var values = req.allParams();

  		if(!values.provinceID || !values.provinceName){
  			return res.json(400, {
	      		error: 'data_missing',
				error_description: 'The provinceID and provinceName is required',
	      	});
  		}
  		Provinces.update({provinceID : values.provinceID},{provinceName : values.provinceName}).exec(function (err, province){
	  		if (err) return res.serverError(err);
	  		return res.json(200,province);
		});

  	},
	
};

