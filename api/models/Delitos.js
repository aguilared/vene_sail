/**
 * Delitos.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    tableName: 'delitos',
    migrate: 'safe',

    attributes: {
	  
        delito_id: {
        	type: "integer", 
        	autoIncrement: true,
        	primaryKey: true
        },
        descripcion: {
        	type: "string",
        	required: true
        }

    }
};
