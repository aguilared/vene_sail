/**
 * Users.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bcrypt = require('bcryptjs');
//var uuid = require('node-uuid');

module.exports = {
  schema: true,
  autoPK: false,
  tableName: "users",
  migrate: "safe",

  attributes: {
    id: {
      type: "string",
      primaryKey: true,
      unique: true,
      index: true,
    },
    name: {
      type: "string",
    },
    email: {
      type: "string",
    },
    password: {
      type: "string",
    },
    remember_token: {
      type: "string",
    },
  },
  comparePassword(password, encryptedPassword) {
    console.log("compare ", password + " " + encryptedPassword);
    return new Promise(function (resolve, reject) {
      bcrypt.compare(password, encryptedPassword, (err, match) => {
        if (err) {
          console.log('compare error ',err)
          return reject("Something went wrong!");
        }
        if (match) return resolve();
        else
          console.log('cno mtch',err); return reject("Mismatch passwords");
      });
    });
  },

};
