/**
 * Provinces.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    tableName: 'profesiones',
    migrate: 'safe',

    attributes: {
	  
        profesion_id: {
        	type: "integer", 
        	primaryKey: true
        },
        descripcion: {
        	type: "string",
        	required: true
        },

    }
};

