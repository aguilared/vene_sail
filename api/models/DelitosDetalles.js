/**
 * Delitos.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    tableName: 'delitos_detalles',
    migrate: 'safe',

    attributes: {
	  	
	  	delito_detalle_id: {
        	type: "integer", 
        	autoIncrement: true,
        	primaryKey: true
        },
        delito_id: {
        	type: "integer", 
        	autoIncrement: true
        	
        },
        descripcion: {
        	type: "string",
        	required: true
        }

    }
};