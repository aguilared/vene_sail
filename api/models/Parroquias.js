/**
 * Provinces.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    tableName: 'parroquias',
    migrate: 'safe',

    attributes: {
	  
        parroquia_id: {
            type: 'integer', 
            primaryKey: true,
            required: true
        },
        municipio_id: {
            type: "integer",
            required: true
        },
        estado_id: {
            type: "integer",
            required: true
        },
        descripcion: {
            type: "text",
            required: true
        },
        latitud: {
            type: 'float'
        },
        longitud: {
            type: 'float'
        }

    }
};



