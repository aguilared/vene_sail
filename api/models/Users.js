/**
 * Users.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bcrypt = require('bcryptjs');
//var uuid = require('node-uuid');

module.exports = {

  schema: true,
  autoPK: false,
  tableName: 'users',
  migrate: 'safe',

  attributes: {

    id: {
      type: "integer",
      autoIncrement: true,
      primaryKey: true,
      required: false
    },
    name: {
      type: 'string',
    },
    email: {
      type: 'string',
    },
    password: {
      type: 'string',
    },
    remember_token: {
      type: 'string',
    }

  },
  // Here we encrypt password before creating a User
  beforeCreate(values, next) {
    console.log('creeando un user', values)
    bcrypt.genSalt(10, (err, salt) => {
      if (err) {
        sails.log.error(err);
        return next();
      }
      console.log('creeando un user no errors')
      bcrypt.hash(values.password, salt, (err, hash) => {
        if (err) {
          sails.log.error(err);
          return next();
        }
        console.log('creeando un user no errors', hash)
        //values.encryptedPassword = hash; // Here is our encrypted password
        values.password = hash; // Here is our encrypted password
        return next();
      });
    });
  },
  comparePassword(password, encryptedPassword) {
    console.log("compare ", password + " " + encryptedPassword);
    return new Promise(function (resolve, reject) {
      bcrypt.compare(password, encryptedPassword, (err, match) => {
        if (err) {
          console.log('compare error ', err)
          return reject("Something went wrong!");
        }
        if (match) {
          console.log('mtch', match);
          return resolve();
        } else {
          console.log('no match', match);
          return reject("Mismatch passwords");
        }
      });
    });
  },

};
