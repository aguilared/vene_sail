/**
 * Provinces.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    tableName: 'coordenadas_parroq',
    migrate: 'safe',

    attributes: {
	  
        id: {
        	type: "integer", 
        	autoIncrement: true,
        	primaryKey: true
        },
        parroquia_id: {
          model: 'Parroquias',
          via: 'parroquia_id'
        },
        lat: {
            type: 'float'
        },
        lng: {
            type: 'float'
        },

    }
};

