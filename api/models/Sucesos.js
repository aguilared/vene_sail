/**
 * Provinces.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: "sucesos",
  migrate: "safe",

  attributes: {
    suceso_id: {
      type: "integer",
      autoIncrement: true,
      primaryKey: true,
      required: false
    },
    fecha_suceso: {
      type: "datetime",
      required: true
    },
    hora: {
      type: "datetime",
      required: true
    },
    titulo: {
      type: "string",
      required: true
    },
    delito_id: {
      model: "Delitos",
      via: "delito_id",
      required: true
    },
    delito_detalle_id: {
      model: "DelitosDetalles",
      via: "delito_detalle_id"
    },
    fuente: {
      type: "string",
      required: true
    },
    otra_fuente1: {
      type: "string",
      required: false
    },
    otra_fuente2: {
      type: "string",
      required: false
    },
    otra_fuente3: {
      type: "string",
      required: false
    },
    nombre_victima: {
      type: "string",
      required: true
    },
    sexo: {
      type: "string",
      required: false
    },
    edad: {
      type: "integer",
      required: false
    },
    profesion_id: {
      type: "integer"
    },
    tipo_arma: {
      type: "integer"
    },
    estado: {
      type: "integer",
      required: true
    },
    municipio_id: {
      type: "integer",
      required: true
    },
    parroquia_id: {
      model: "Parroquias",
      via: "parroquia_id",
      required: true
    },
    latitud: {
      type: "float"
    },
    longitud: {
      type: "float"
    },
    sector: {
      type: "string"
    },
    usuario: {
      type: "integer"
    },
    mi_resena: {
      type: "string"
    },
    fecha_creado: {
      type: "datetime"
    }
  }
};

